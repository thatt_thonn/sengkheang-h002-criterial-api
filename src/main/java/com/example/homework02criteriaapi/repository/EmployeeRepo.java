package com.example.homework02criteriaapi.repository;

import com.example.homework02criteriaapi.entities.Account;
import com.example.homework02criteriaapi.entities.Department;
import com.example.homework02criteriaapi.entities.Employee;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Tuple;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Repository
@Transactional
public class EmployeeRepo {

    @PersistenceContext
    private EntityManager entityManager;


    /*
    1. Get All Employee
    */
    public List<Employee> getAllEmployee() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Employee> criteriaQuery = criteriaBuilder.createQuery(Employee.class);
        Root<Employee> root = criteriaQuery.from(Employee.class);
        criteriaQuery.select(root);
        Order order=criteriaBuilder.asc(root.get("empName"));
        criteriaQuery.orderBy(order);
        return entityManager.createQuery(criteriaQuery).getResultList();

    }

    /*
    2. get all employee by department's id
    */
    public List<Employee> getEmployeeByDepartmentId(int id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Employee> criteriaQuery = criteriaBuilder.createQuery(Employee.class);
        Root<Employee> root = criteriaQuery.from(Employee.class);
        Join<Employee, Department> departmentJoin = root.join("department");
        criteriaQuery.select(root).where(criteriaBuilder.equal(departmentJoin.get("id"), id));

        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    /*
    3. get all employee by Id
    */
    public List<Employee> getEmployeeById(int id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Employee> criteriaQuery = criteriaBuilder.createQuery(Employee.class);
        Root<Employee> root = criteriaQuery.from(Employee.class);
        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("id"), id));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    /*
    4. sum all employee's salary
    */
    public List<Tuple> sumAllSalary() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = criteriaBuilder.createTupleQuery();
        Root<Employee> root = criteriaQuery.from(Employee.class);
        Join<Employee, Department> joinSalary = root.join("department");
        criteriaQuery.select(criteriaBuilder.tuple(criteriaBuilder.sum(root.get("salary"))));
        criteriaQuery.groupBy(joinSalary);
        Query query = entityManager.createQuery(criteriaQuery);
        List<Tuple> employees = query.getResultList();
        return employees;
    }

    /*
    5. Wrap into One object
    */
    public List<Tuple> wrapEmployeeIntoObject(){
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery=criteriaBuilder.createQuery(Tuple.class);
        Root<Employee> employeeRoot=criteriaQuery.from(Employee.class);
        criteriaQuery.multiselect(employeeRoot.get("id"), employeeRoot.get("empName"), employeeRoot.get("gender"), employeeRoot.get("salary"));
        List<Tuple> allEmployees = entityManager.createQuery(criteriaQuery).getResultList();
        return allEmployees;
    }

    /*
    6. select all department with specification
    */
    public List<Tuple> selectAllDepartment(){
        CriteriaBuilder cb=entityManager.getCriteriaBuilder();
        CriteriaQuery<Tuple> cq=cb.createQuery(Tuple.class);
        Root<Department> department=cq.from(Department.class);
        cq.multiselect(department.get("id"),department.get("departmentName"),cb.count(department.get("id"))).groupBy(department.<Employee>get("id"));
        List<Tuple> allEmp=entityManager.createQuery(cq).getResultList();
        return allEmp;
    }


    /*
    7. get max value of Employee's salary
    */
    public Double getMaxSalary(){
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaQuery<Double> criteriaQuery=criteriaBuilder.createQuery(Double.class);
        Root<Employee> employeeRoot=criteriaQuery.from(Employee.class);
        criteriaQuery.select(criteriaBuilder.max(employeeRoot.get("salary")));
        Double d=entityManager.createQuery(criteriaQuery).getSingleResult();
        return d ;
    }

    /*
    8. get salary between 300 to 500
    */
    public List<Employee> getSalaryBetween(Double min, Double max){
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Employee> criteriaQuery=criteriaBuilder.createQuery(Employee.class);
        Root<Employee> employeeRoot=criteriaQuery.from(Employee.class);
        criteriaQuery.select(employeeRoot).where(criteriaBuilder.between(employeeRoot.get("salary"), min, max));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    /*
    9. update employee's salary by department
    */
    public Integer setEmployeeSalaryByDepartment(int id){
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaUpdate<Employee> criteriaUpdate=criteriaBuilder.createCriteriaUpdate(Employee.class);
        Root<Employee> employeeRoot=criteriaUpdate.from(Employee.class);
        criteriaUpdate.set(employeeRoot.get("salary"), 600);
        criteriaUpdate.where(criteriaBuilder.equal(employeeRoot.get("department").get("id"), id));
        return entityManager.createQuery(criteriaUpdate).executeUpdate();
    }

    /*
    10. update employee's info by id
    */
    public Integer updateEmployeeInfoById(int id){
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaUpdate<Employee> criteriaUpdate=criteriaBuilder.createCriteriaUpdate(Employee.class);
        Root<Employee> root=criteriaUpdate.from(Employee.class);
        criteriaUpdate.set(root.get("empName"), "Maritnette");
        criteriaUpdate.where(criteriaBuilder.equal(root.get("id"), id));
        return entityManager.createQuery(criteriaUpdate).executeUpdate();
    }

    /*
    10. delete employee's record
    */
    public Boolean deleteEmployeefromAccountById(int id){
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaDelete delete=criteriaBuilder.createCriteriaDelete(Employee.class);
        Root account=delete.from(Account.class);
        delete.where(criteriaBuilder.equal(account.get("employee").get("id"), id));
        int query = entityManager.createQuery(delete).executeUpdate();
        return true;
    }

    public Boolean deleteEmployeeById(int id){
        deleteEmployeefromAccountById(id);
        CriteriaBuilder criteriaBuilder=entityManager.getCriteriaBuilder();
        CriteriaDelete delete1=criteriaBuilder.createCriteriaDelete(Employee.class);
        Root employee=delete1.from(Employee.class);
        delete1.where(criteriaBuilder.equal(employee.get("id"),id));
        Integer query=entityManager.createQuery(delete1).executeUpdate();
        return true;
    }
}
